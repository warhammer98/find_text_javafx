package lifelesshub.icu;


import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.DirectoryChooser;

import java.io.File;
import java.util.*;

public class MainMenuController {

    @FXML
    private Button startSearchButton;
    @FXML
    private TextField searchStringField;
    @FXML
    private TextField searchPathField;
    @FXML
    private TextField fileExtField;
    @FXML
    private TabPane fileTabs;
    @FXML
    private TreeView<String> fileTree;

    private ArrayList<TabInfo> tabInfos = new ArrayList<>();

    private Main main;

    private class TabInfo {
        private File file;
        private Tab tab;
        private Integer matchPointer;
        private ArrayList<Integer> matchIndexes = new ArrayList<>();
        private int linesCount;
        private ScrollPane scrollPane;

        TabInfo(File file) {
            TabInfo self = this;
            this.file = file;
            this.scrollPane = new ScrollPane(new VBox());
            this.tab = new Tab(file.getName(), scrollPane);
            tab.setOnCloseRequest(e -> tabInfos.remove(self));
            // открытие файла делаем в новом потоке
            Task task = new Task<Void>() {
                @Override
                public Void call() {
                    tabInfos.add(self);
                    loadFileContent();
                    return null;
                }
            };
            Thread thread = new Thread(task);
            thread.start();
        }

        private void loadFileContent() {
            try {
                Scanner scanner = new Scanner(file);
                linesCount = 0;
                String searchString = searchStringField.getText();
                // пока не достигнут конец файла
                while (scanner.hasNextLine()) {
                    // читаем следующую строку
                    String line = scanner.nextLine();
                    boolean matchFound = line.contains(searchString);
                    TextFlow textFlowPane = new TextFlow();
                    // разбиваем строку на части с разделителем, равным поисковому запросу
                    List<String> parts = Arrays.asList(line.split(searchString));
                    // для каждой части разбиения
                    parts.forEach(s -> {
                                // добавляем ее в текст
                                Text token = new Text(s);
                                textFlowPane.getChildren().add(token);
                                if (matchFound) {
                                    // если в строке есть совпадение с запросом
                                    // то после токена вставляем запрос, окрашивая его в красный цвет
                                    Text match = new Text(searchString);
                                    match.setFill(Color.RED);
                                    textFlowPane.getChildren().add(match);
                                }
                            });
                    if (matchFound) {
                        // если в строке есть совпадение, удаляем последний элемент TextFlow
                        // - это будет лишняя подстрока-запрос
                        textFlowPane.getChildren().remove(textFlowPane.getChildren().size()-1);
                    }
                    Thread.sleep(1);
                    // UI обновляем в Platform.runLater()
                    Platform.runLater(() -> {
                        // добавляем в контент
                        ((VBox) scrollPane.getContent()).getChildren().add(textFlowPane);
                    });
                    if (matchFound) {
                        // добавляем номер строки в массим индексов совпадений
                        matchIndexes.add(linesCount);
                        // если это первое совпадение, инициализируем matchPointer
                        if (matchPointer == null) {
                            matchPointer = 0;
                        }
                    }
                    linesCount++;
                }
            }
            catch (Exception ignored) { }
        }

    }

    public void init() {
        SelectionModel<TreeItem<String>> selectionModel = fileTree.getSelectionModel();
        // устанавлиаем обработчик события выбора файла в дереве
        selectionModel.selectedItemProperty().addListener((changed, oldValue, newValue) -> {
            if (newValue != null) {
                String path = getFilePath(newValue);
                File file = new File(path);
                // если выбран файл
                if (file.isFile()) {
                    //создаем новый экземляр TabInfo
                    TabInfo tabInfo = new TabInfo(file);
                    // и добавляем вкладку в TabPane
                    fileTabs.getTabs().add(tabInfo.tab);
                    SingleSelectionModel<Tab> tabSelectionModel = fileTabs.getSelectionModel();
                    // и устанавливаем фокус на нее
                    tabSelectionModel.select(tabInfo.tab);
                }
            }
        });
    }

    private boolean fileContainsWord(String fileName, String word)  {
        try {
            Scanner scanner = new Scanner(new File(fileName));
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if(line.contains(word)) {
                    return true;
                }
            }
            return false;
        }
        catch (Exception e) {
            return false;
        }
    }

    private TreeItem<String> recursiveSearch(File root, final String searchString, final String[] fileExt, boolean isRoot) {
        if (root != null) {
            TreeItem<String> rootTreeNode;
            if (isRoot) {
                // если текущий файл(папка) - root, то в дереве будет абсолютный путь
                rootTreeNode = new TreeItem<>(root.getAbsolutePath());
            }
            else {
                // иначе - название
                rootTreeNode = new TreeItem<>(root.getName());
            }
            File[] files = root.listFiles();
            if (files != null) {
                for (File file: files) {
                    if (file.isDirectory()) {
                        // если это папка - рекурсивно ищем в ней файлы и папки
                        TreeItem<String> child = recursiveSearch(file, searchString, fileExt, false);
                        if (child != null) {
                            rootTreeNode.getChildren().add(child);
                        }
                    }
                    else {
                        // если нет - вытягиваем расширение
                        String[] nameParts = file.getName().split("\\.");
                        String curFileExt;
                        if (nameParts.length > 0)
                        {
                             curFileExt = nameParts[nameParts.length-1];
                        }
                        else {
                            curFileExt = "";
                        }
                        // проверяем расширение файла, если оно соответствует указанным - ищем поисковый запрос
                        if ((Arrays.asList(fileExt).contains(curFileExt)) && fileContainsWord(file.toString(),searchString)) {
                            // если все хорошо - добавляем файл в дерево
                            rootTreeNode.getChildren().add(new TreeItem<>(file.getName()));
                        }
                    }
                }
                // если что-нибудь нашлось - добавляем узел в дерево
                if (rootTreeNode.getChildren().size() != 0) {
                    return rootTreeNode;
                }

            }
        }
        return null;
    }

    public void search() {

        // тянем данные для поиска
        final String searchPath = searchPathField.getText();
        final String searchString = searchStringField.getText();
        final String[] fileExt;
        if (fileExtField.getText().trim().length() > 0)
            fileExt = fileExtField.getText().split(",");
        else
            fileExt = new String[]{"log"};

        if (searchPath.length() > 0 && searchString.length() > 0) {
            startSearchButton.setText("Processing");
            // для того, чтобы UI не зависал на время поиска, создаем новый поток
            Task task = new Task<Void>() {
                @Override
                public Void call() {
                    File root = new File(searchPath);
                    TreeItem<String> rootTreeNode = recursiveSearch(root, searchString, fileExt, true);
                    // UI обновляем в Platform.runLater()
                    Platform.runLater(() -> {
                        initTree(rootTreeNode);
                        startSearchButton.setText("Start search");
                    });
                    return null;
                }
            };
            new Thread(task).start();
        }
    }

    private void initTree(TreeItem<String> root) {
        // если ничего не нашлось - показываем сообщение о том, что ничего не найдено
        fileTree.setRoot(Objects.requireNonNullElseGet(root, () -> new TreeItem<>("No files found")));
    }


    public void prevMatch() {
        // получаем индекс текущей вкладки
        int selectedTabIdx = fileTabs.getSelectionModel().getSelectedIndex();
        // получам информацию о вкладке по индексу
        TabInfo selectedTabInfo = tabInfos.get(selectedTabIdx);
        if (selectedTabInfo.tab != null && selectedTabInfo.matchPointer != null && selectedTabInfo.matchPointer > 0) {
            Integer prevIdx = selectedTabInfo.matchIndexes.get(--selectedTabInfo.matchPointer);
            // вычисляем процент, на который нужно прокрутить scrollPane,
            // чтобы увидеть очередное совпадение
            double position = (double)prevIdx / selectedTabInfo.linesCount;
            selectedTabInfo.scrollPane.setVvalue(position);
        }
    }
    public void nextMatch() {
        // получаем индекс текущей вкладки
        int selectedTabIdx = fileTabs.getSelectionModel().getSelectedIndex();
        // получаем информацию о вкладке по индексу
        TabInfo selectedTabInfo = tabInfos.get(selectedTabIdx);
        if (selectedTabInfo.tab != null && selectedTabInfo.matchPointer != null && selectedTabInfo.matchPointer < selectedTabInfo.matchIndexes.size()-1)  {
            Integer nextIdx = selectedTabInfo.matchIndexes.get(++selectedTabInfo.matchPointer);
            // вычисляем процент, на который нужно прокрутить scrollPane,
            // чтобы увидеть очередное совпадение
            double position = (double) nextIdx/ selectedTabInfo.linesCount;
            selectedTabInfo.scrollPane.setVvalue(position);
        }
    }
    private String getFilePath(TreeItem<String> item) {
        String os = System.getProperty("os.name");
        TreeItem<String> parent = item.getParent();
        StringBuilder path = new StringBuilder(item.getValue());
        // тянем предков текущего файла
        while(parent != null){
            // если запускаем из под Windows, то слеши обратные
            if (os.contains("Windows"))
                path.insert(0, parent.getValue() + "\\");
            else
                path.insert(0, parent.getValue() + "/");
            parent = parent.getParent();
        }
        return path.toString();
    }

    public void chooseFile() {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Choose folder to search in");
        File selectedDirectory = chooser.showDialog(main.getPrimaryStage());
        System.out.println(selectedDirectory.getAbsolutePath());
        searchPathField.setText(selectedDirectory.getAbsolutePath());
    }


    public void setMain(Main main) {
        this.main = main;
    }
}
