module lifelesshub.icu {
    requires javafx.controls;
    requires javafx.fxml;

    opens lifelesshub.icu to javafx.fxml;
    exports lifelesshub.icu;
}